﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HamsterLib
{
    /// <summary>
    /// The player character(s) in the RPG.
    /// </summary>
    public class Hero
    {
        /// <summary>
        /// The hero's name.
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                if (value.Length > nameMaxLength && nameMaxLength != 0)
                {
                    //truncate the name if it's over maximum length
                    value = value.Substring(0, nameMaxLength);
                }
                name = value;
            }
        }


        /// <summary>
        /// The maximum name length when using the rename system.
        /// </summary>
        /// <remarks>If zero, use the engine default.</remarks>
        public short NameMaxLength
        {
            get
            {
                return nameMaxLength;
            }

            set
            {
                nameMaxLength = value;
            }
        }

        /// <summary>
        /// The hero's "walkabout" (map) graphics.
        /// </summary>
        public SpriteReference WalkaboutGfx
        {
            get
            {
                return walkaboutGfx;
            }

            set
            {
                walkaboutGfx = value;
            }
        }

        /// <summary>
        /// The hero's battle graphics.
        /// </summary>
        public SpriteReference BattleGfx
        {
            get
            {
                return battleGfx;
            }

            set
            {
                battleGfx = value;
            }
        }

        /// <summary>
        /// The hero's "hand" location for item placement in attack frame A.
        /// </summary>
        public Point HandA
        {
            get
            {
                return handA;
            }

            set
            {
                handA = value;
            }
        }
        
        /// <summary>
        /// The hero's "hand" location for item placement in attack frame B.
        /// </summary>
        public Point HandB
        {
            get
            {
                return handB;
            }

            set
            {
                handB = value;
            }
        }

        /// <summary>
        /// The hero's portrait picture.
        /// </summary>
        /// <remarks>
        /// This is used in the Status menu and text boxes.
        /// </remarks>
        public SpriteReference Portrait
        {
            get
            {
                return portrait;
            }

            set
            {
                portrait = value;
            }
        }

        /// <summary>
        /// The level the hero is at when it joins the party.
        /// </summary>
        public short DefaultLevel
        {
            get
            {
                return defaultLevel;
            }

            set
            {
                defaultLevel = value;
            }
        }

        /// <summary>
        /// The hero's weapon when no Weapon is equipped.
        /// </summary>
        public short DefaultWeapon
        {
            get
            {
                return defaultWeapon;
            }

            set
            {
                defaultWeapon = value;
            }
        }

        /// <summary>
        /// A List containing the hero's battle menu items.
        /// </summary>
        public List<BattleMenuItem> BattleMenuItems
        {
            get
            {
                return battleMenuItems;
            }

            set
            {
                battleMenuItems = value;
            }
        }

        /// <summary>
        /// A dictionary containing the multipliers done to elemental damage.
        /// If the element is not in the dictionary, the engine assumes 1.0.
        /// </summary>
        public Dictionary<short, float> ElementMultipliers
        {
            get
            {
                return elementMultipliers;
            }

            set
            {
                elementMultipliers = value;
            }
        }

        /// <summary>
        /// If true, shows a rename screen for the hero when added to party.
        /// </summary>
        public bool RenameOnAdd
        {
            get
            {
                return renameOnAdd;
            }

            set
            {
                renameOnAdd = value;
            }
        }

        /// <summary>
        /// If true, the hero can be renamed at any time from the Stats menu.
        /// </summary>
        public bool RenameAnytime
        {
            get
            {
                return renameAnytime;
            }

            set
            {
                renameAnytime = value;
            }
        }

        public bool HideEmptyLists
        {
            get
            {
                return hideEmptyLists;
            }

            set
            {
                hideEmptyLists = value;
            }
        }

        /// <summary>
        /// The tags to turn ON when certain conditions are true.
        /// <see cref="TagType"/> for the conditions.
        /// </summary>
        public Dictionary<TagType, short> Tags
        {
            get
            {
                return tags;
            }

            set
            {
                tags = value;
            }
        }

        public Dictionary<OHRRPGCE.Stat, bool> HiddenStats
        {
            get
            {
                return hiddenStats;
            }

            set
            {
                hiddenStats = value;
            }
        }

        private string name;

        private short nameMaxLength;

        private SpriteReference walkaboutGfx;

        private SpriteReference battleGfx;

        private Point handA;

        private Point handB;

        private SpriteReference portrait;

        private short defaultLevel;

        private short defaultWeapon;

        //Remember to initialize these in the constructor.
        private short[,] stats = new short[OHRRPGCE.LastStat, 2];

        private List<BattleMenuItem> battleMenuItems;

        //Name, spell list entry, random spell select, use FF1-style
        private SpellList[] spellLists = new SpellList[4];

        private Dictionary<short, float> elementMultipliers;

        private bool renameOnAdd;

        private bool renameAnytime;

        private bool hideEmptyLists;

        private Dictionary<TagType, short> tags;

        private Dictionary<OHRRPGCE.Stat, bool> hiddenStats;

        //Enums we need for Heroes
        /// <summary>
        /// Hero tag conditions.
        /// </summary>
        public enum TagType
        {
            /// <summary>
            /// The hero is in the party or in reserve.
            /// </summary>
            HaveHero,

            /// <summary>
            /// The hero is alive (HP > 0).
            /// </summary>
            IsAlive,

            /// <summary>
            /// The hero is the party leader (in caterpillar slot 0).
            /// </summary>
            IsLeader,

            /// <summary>
            /// The leader is in the active party now.
            /// </summary>
            IsActive
        }
        
        public enum MenuType
        {
            Weapon, Attack, Item, SpellList
        }

        public enum SpellListType
        {
            /// <summary>
            /// The most commonly used spell list type. A simple list of attacks to be picked from.
            /// </summary>
            Normal,

            /// <summary>
            /// Emulates Final Fantasy I's unusual spell type. Each row uses a shared pool 
            /// of Level MP types.
            /// </summary>
            FF1Style,

            /// <summary>
            /// Doesn't display a menu, but chooses a random spell from the list when picked in battle.
            /// </summary>
            Random
        }

        /// <summary>
        /// Creates a new default hero.
        /// </summary>
        public Hero()
        {
            //this is a blank hero, use defaults.
            name = "";
            walkaboutGfx.Palette = -1;
            battleGfx.Palette = -1;
            portrait.Palette = -1;

            battleMenuItems.Add(new BattleMenuItem(MenuType.Weapon));
            for (int i = 0; i < 4; i++)
            {
                battleMenuItems.Add(new BattleMenuItem(MenuType.SpellList, (short)i, 0));
            }
            battleMenuItems.Add(new BattleMenuItem(MenuType.Item));
        }

        /// <summary>
        /// Creates a hero from the heroes.reld file.
        /// </summary>
        /// <param name="nodeIn">The hero node from the heroes.reld tree.</param>
        public Hero(ReloadNode nodeIn)
        {
            //TODO: write conversion code from RELOAD to HamsterLib
            throw new NotImplementedException();
        }

        public short GetStat(OHRRPGCE.Stat stat, OHRRPGCE.AtHeroLevel lv)
        {
            return stats[(int)stat, (int)lv];
        }

        public void SetStat(OHRRPGCE.Stat stat, OHRRPGCE.AtHeroLevel lv, short value)
        {
            //TODO: write method to set stat in stat array
            stats[(int)stat, (int)lv] = value;
        }

        public ReloadNode SerializeHero()
        {
            throw new NotImplementedException();
        }

        public static List<Hero> ReadFromFile(string HeroFile)
        {
            throw new NotImplementedException();
        }

        public struct BattleMenuItem
        {
            /// <summary>
            /// The type of item in the menu.
            /// </summary>
            public MenuType type;
            /// <summary>
            /// If type is Attack, the Attack ID number. If type is SpellList, the spell list ID.
            /// Ignore if type is Weapon or Item.
            /// </summary>
            public short idNo;
            /// <summary>
            /// This menu should only appear if the tag conditional matches. (Discard RELOAD node if 0.)
            /// </summary>
            public short appearIfTag;

            /// <summary>
            /// Creates a new battle menu item.
            /// </summary>
            /// <param name="p1">The menu item type.</param>
            /// <param name="p2">If type is Attack, the Attack ID number. If type is SpellList, the spell list ID.
            /// Ignored if type is Weapon or Item.</param>
            /// <param name="p3">The tag conditional for the item to appear.</param>
            public BattleMenuItem(MenuType p1, short p2, short p3)
            {
                type = p1;
                idNo = p2;
                appearIfTag = p3;
            }

            /// <summary>
            /// Creates a new battle menu item with ID reference and tag set to zero.
            /// </summary>
            /// <param name="p1">The menu item type.</param>
            public BattleMenuItem(MenuType p1)
            {
                type = p1;
                idNo = 0;
                appearIfTag = 0;
            }
        }

        public struct SpellList
        {
            public string name;
            public List<SpellListEntry> entries;
            public SpellListType listType;

            public SpellList(string p1, SpellListType p2)
            {
                name = p1;
                listType = p2;
                entries = new List<SpellListEntry>(24);
            }
        }

        public struct SpellListEntry
        {
            public short attack;
            public short learnLevel;
            public bool learnFromItem;
            //public short tag; //looks like it's not used

            public SpellListEntry(short p1, short p2, bool p3 = false)
            {
                attack = p1;
                learnLevel = p2;
                learnFromItem = p3;
            }
            
        }
    }
}
