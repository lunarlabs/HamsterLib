 /*
 * RELOAD related functions
 * ------------------------
 * 
 * Don't poke the nodes too much or they'll bite.
 */ 

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HamsterLib
{
    /// <summary>
    /// Constants used by the RELOAD system.
    /// </summary>
    public static class Reload
    {
        /// <summary>
        /// The string "RELD". All RELOAD files have this as the first four bytes.
        /// </summary>
        public const int MagicNumber = 0x444c4552;
        // "RELD" ... if it doesn't have this as the first 4 bytes it's NOT a reload file!

        /// <summary>
        /// The latest RELOAD version that can be handled.
        /// </summary>
        public const byte VersionNumber = 1;
        // Remember to keep up to date with the email list about RELOAD developments.

        /// <summary>
        /// The size of the RELOAD header.
        /// </summary>
        public const int HeaderSize = 13;
        // Why a int32? Ask TMC or whoever made the RELOAD spec...

        /// <summary>
        /// The type a RELOAD node can have.
        /// </summary>
        public enum NodeType
        {
            /// <summary>
            /// Defines a node with no data, to be used as a flag or container.
            /// </summary>
            Null,

            /// <summary>
            /// Defines a node that holds an integer up to 64 bits.
            /// </summary>
            /// <remarks>The on-disk specification holds several sizes of integer, however in memory they can be used interchangably.</remarks>
            Int,

            /// <summary>
            /// Defines a node that holds a double-precision float.
            /// </summary>
            Double,

            /// <summary>
            /// Defines a node that holds a string or a blob of bytes.
            /// </summary>
            String
        }
    }

    /// <summary>
    /// A node containing RELOAD data, including children.
    /// </summary>
    public class ReloadNode
    {
        //Declarations
        private Reload.NodeType _type;
        public Reload.NodeType Type
        {
            get
            {
                return _type;
            }
        }

        private string _nodeName;
        public string Name
        {
            get
            {
                return _nodeName;
            }
            set
            {
                _nodeName = value;
            }
        }

        private long _intData;

        /// <summary>
        /// Gets or sets a node's integer data if the node is of the type Byte, Int16, Int32, or Int64.
        /// </summary>
        /// <exception cref="OverflowException">Thrown when the value to be stored exceeds the capacity of the node type.</exception>
        /// <exception cref="InvalidOperationException">Thrown when the node type does not contain an integer.</exception>
        public long IntData
        {
            get
            {
                if (_type == Reload.NodeType.Int)
                {
                    return _intData;
                }
                else
                {
                    string err = "Wrong node type: Tried to read Int value from a " + _type.ToString() + " node.";
                    throw new InvalidOperationException(err);
                }
            }
            set
            {
                if (_type == Reload.NodeType.Int)
                {
                    _intData = value;
                }
                else
                {
                    string err = "Wrong node type: tried to pass a Int value to a " + _type.ToString() + " node.";
                    throw new InvalidOperationException(err);
                }
            }
        }

        private double _doubleData;
        /// <summary>
        /// A double node's data.
        /// </summary>
        public double DoubleData
        {
            get
            {
                if (_type == Reload.NodeType.Double)
                {
                    return _doubleData;
                }
                else
                {
                    string err = "Wrong node type: Tried to read Double value from a " + _type.ToString() + " node.";
                    throw new InvalidOperationException(err);
                }
            }
            set
            {
                if (_type == Reload.NodeType.Double)
                {
                    _doubleData = value;
                }
                else
                {
                    string err = "Wrong node type: tried to pass a Double value to a " + _type.ToString() + " node.";
                    throw new InvalidOperationException(err);
                }
            }
        }

        
        private string _stringData;
        /// <summary>
        /// A string node's data.
        /// </summary>
        public string StringData
        {
            get
            {
                if (_type == Reload.NodeType.String)
                {
                    return _stringData;
                }
                else
                {
                    string err = "Wrong node type: Tried to read String value from a " + _type.ToString() + " node.";
                    throw new InvalidOperationException(err);
                }
            }
            set
            {
                if (_type == Reload.NodeType.String)
                {
                    _stringData = value;
                }
                else
                {
                    string err = "Wrong node type: tried to pass a String value to a " + _type.ToString() + " node.";
                    throw new InvalidOperationException(err);
                }
            }
        }

        private List<ReloadNode> _children = new List<ReloadNode>();

        /// <summary>
        /// A List containing the node's children.
        /// </summary>
        /// <remarks>Instead of using Children.Add and Children.Insert, use addChild and insertChild
        /// to properly set the parent.</remarks>
        public List<ReloadNode> Children
        {
            get
            {
                return _children;
            }
        }

        /// <summary>
        /// The number of children the node has.
        /// </summary>
        public int NumberOfChildren
        {
            get
            {
                return _children.Count;
            }
        }

        private ReloadNode parent = null;
        /// <summary>
        /// A reference to the node's parent in the tree, or null if it is the root node.
        /// </summary>
        public ReloadNode Parent
        {
            get
            {
                return parent;
            }
        }

        public string toString()
        {
            return _nodeName;
        }

        //constructors

        /// <summary>
        /// Creates a Null ReloadNode.
        /// </summary>
        public ReloadNode()
        {
            //No argument means a null node type.
            _type = Reload.NodeType.Null;
        }

        /// <summary>
        /// Creates a Integer ReloadNode.
        /// </summary>
        /// <param name="lData">The node's data.</param>
        public ReloadNode(long lData)
        {
            _type = Reload.NodeType.Int;
            _intData = lData;
        }

        /// <summary>
        /// Creates a Double-Precision Float ReloadNode.
        /// </summary>
        /// <param name="dData">The node's data.</param>
        public ReloadNode(double dData)
        {
            _type = Reload.NodeType.Double;
            _doubleData = dData;
        }

        /// <summary>
        /// Creates a String ReloadNode.
        /// </summary>
        /// <param name="sData">The node's data.</param>
        public ReloadNode(string sData)
        {
            _type = Reload.NodeType.String;
            _stringData = sData;
        }

        /// <summary>
        /// Checks to see if a ReloadNode has an immediate child with a certain name.
        /// </summary>
        /// <param name="name">The name of the child to look for.</param>
        /// <returns>Returns true if there is an immediate child with the name, or false otherwise.</returns>
        public bool hasChild(string name)
        {
            return (getFirstPositionOfChildNamed(name) > 1);
        }

        public int[] getPositionsOfChildrenNamed(string name)
        {
            int[] result = new int[_children.Count];
            int matches = 0;
            for (int i = 0; i < _children.Count; i++)
            {
                if (_children[i].Name == name)
                {
                    result[matches++] = i;
                }
            }
            Array.Resize(ref result, matches);
            return result;
        }

        /// <summary>
        /// Returns the position of node's immediate named child.
        /// </summary>
        /// <param name="name">The name of the child to look for</param>
        /// <returns>The position of the child in children, or -1 if the child was not found.</returns>
        /// <remarks>This only returns the position of the first child with the name.</remarks>
        public int getFirstPositionOfChildNamed(string name)
        {
            for (int i = 0; i < _children.Count; i++)
            {
                if (_children[i].Name == name)
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Returns a node's immediate named child.
        /// </summary>
        /// <param name="name">The name of the child to get.</param>
        /// <returns>The named child, or <c>null</c> if there is no child with that name.</returns>
        /// <remarks>This only returns the first child with the node name.</remarks>
        public ReloadNode getChild(string name)
        {
            for (int i = 0; i < _children.Count; i++)
            {
                if (_children[i].Name == name)
                {
                    return _children[i];
                }
            }
            return null;
        }

        /// <summary>
        /// Returns an immediate child node.
        /// </summary>
        /// <param name="index">The number of the child to get</param>
        /// <returns>The child node.</returns>
        public ReloadNode getChild(int index)
        {
            return _children[index];
        }

        public void addChild(ReloadNode node)
        {
            node.parent = this;
            _children.Add(node);
        }

        public void insertChild(ReloadNode node, int index)
        {
            node.parent = this;
            _children.Insert(index, node);
        }
    }

    /// <summary>
    /// Performs operations on Reload files.
    /// </summary>
    public static class ReloadFileIO
    {
        private static List<String> nameTable;
        /// <summary>
        /// Reads a RELOAD file and turns it into a ReloadNode hierarchy.
        /// </summary>
        /// <param name="reloadFileLocation">The file path of the RELOAD file.</param>
        /// <param name="careAboutIntType">If the import process should care what type an integer node is when importing.</param>
        /// <returns>A ReloadNode containing the node structure of the RELOAD file.</returns>
        public static ReloadNode readReloadFile(string reloadFileLocation)
        {
            ReloadNode result = null;
            
            int stringTablePosition;

            if (File.Exists(reloadFileLocation))
            {
                Debug.WriteLine("Checking header of " + reloadFileLocation + "...");
                using (FileStream inFile = new FileStream(reloadFileLocation, FileMode.Open))
                {
                    if (inFile.Length < Reload.HeaderSize)
                    {
                        throw new EndOfStreamException("File is too small to be a RELOAD file");
                    }
                    using (BinaryReader b = new BinaryReader(inFile))
                    {
                        int magicNumberCheck = b.ReadInt32();
                        if (magicNumberCheck != Reload.MagicNumber)
                        {
                            throw new FormatException(reloadFileLocation + " is not a RELOAD file!");
                        }

                        byte versionCheck = b.ReadByte();
                        if (versionCheck != Reload.VersionNumber)
                        {
                            throw new FormatException(reloadFileLocation + " has RELOAD version " + versionCheck
                                + ", needed " + Reload.VersionNumber);
                        }

                        int headerLengthCheck = b.ReadInt32();
                        if (headerLengthCheck != Reload.HeaderSize)
                        {
                            throw new FormatException("Found header length " + headerLengthCheck + ", was expecting "
                                + Reload.HeaderSize);
                        }

                        stringTablePosition = b.ReadInt32();
                        Debug.WriteLine("Looks sane. Node name offset at " + stringTablePosition);

                        nameTable = readNameTable(b);

                        result = readIn(b);
                    }
                }
                Debug.WriteLine("");
                Debug.WriteLine("Done reading RELOAD file, no errors.");
                return result;
            }
            else
            {
                throw new FileNotFoundException("Can't find the RELOAD file:" + reloadFileLocation);
            }

            List<string> readNameTable(BinaryReader br)
            {
                List<string> ntresult = new List<string>();
                long flashback = br.BaseStream.Position;
                br.BaseStream.Seek(stringTablePosition, SeekOrigin.Begin);
                StringBuilder sb = new StringBuilder();
                ntresult.Add(""); //String zero is always the empty string, but is not stored in the file
                int count = readVLI(br);
                for (int i = 0; i < count; i++)
                {
                    sb.Clear();
                    int characters = readVLI(br);
                    for (int j = 0; j < characters; j++)
                    {
                        sb.Append((char)br.ReadByte());
                    }
                    ntresult.Add(sb.ToString());
                }
                br.BaseStream.Seek(flashback, SeekOrigin.Begin);
                return ntresult;
            }

            ReloadNode readIn(BinaryReader br)
            {
                ReloadNode nodeResult = null;

                int nodeLength = br.ReadInt32();
                //            Debug.Write("I see a new node, length " + nodeLength);

                int nameID = readVLI(br);
                //            Debug.Write(", ID is " + nameID);

                int type = br.ReadByte();
                //           Debug.WriteLine(", type is " + type + "...");

                //create the node based on data type
                switch (type)
                {
                    case 0:
                        nodeResult = new ReloadNode();
                        break;
                    case 1:
                        nodeResult = new ReloadNode(br.ReadByte());
                        break;
                    case 2:
                        nodeResult = new ReloadNode(br.ReadInt16());
                        break;
                    case 3:
                        nodeResult = new ReloadNode(br.ReadInt32());
                        break;
                    case 4:
                        nodeResult = new ReloadNode(br.ReadInt64());
                        break;
                    case 5:
                        nodeResult = new ReloadNode(br.ReadDouble());
                        break;
                    case 6:
                        int strLen = readVLI(br);
                        if (strLen > nodeLength)
                        {
                            throw new InvalidDataException("String length is bad or pointer is misplaced.");
                        }
                        StringBuilder sb = new StringBuilder(strLen);
                        for (int i = 0; i < strLen; i++)
                        {
                            sb.Append((char)br.ReadByte());
                        }
                        nodeResult = new ReloadNode(sb.ToString());
                        break;
                    default:
                        Debug.Write(" I don't know how to handle it");
                        throw new InvalidDataException("Don't know how to handle type " + type);
                }
                /*
                //jump to name table
                long position = br.BaseStream.Position;
                br.BaseStream.Seek(stringTablePosition, SeekOrigin.Begin);

                //sanity check
                if (nameID > readVLI(br))
                {
                    throw new InvalidDataException("Called non-existent string ID or string table counter is incorrect");
                }

                for (int i = 1; i < nameID; i++)
                {
                    //skip other strings until we get to our name
                    br.BaseStream.Seek((long)readVLI(br), SeekOrigin.Current);
                }
                int nameLen = readVLI(br);
                StringBuilder nodeName = new StringBuilder(nameLen);
                for (int i = 0; i < nameLen; i++)
                {
                    nodeName.Append((char)br.ReadByte());
                }
                */
                nodeResult.Name = nameTable[nameID];
                Debug.Write(nodeResult.Name + ", ");
                int children = readVLI(br);
                if (children > 0)
                /*            {
                                Debug.WriteLine("it has no children.");
                            }
                            else*/
                {
                    //                Debug.WriteLine(children + " subnodes.");
                    for (int i = 0; i < children; i++)
                    {
                        //                    Debug.WriteLine("Child " + (i + 1) + " of " + children + ":");
                        ReloadNode child = readIn(br);
                        nodeResult.addChild(child);
                    }
                    //                Debug.WriteLine("Finished getting children of " + result.Name);
                }
                return nodeResult;
            }
        }


        /// <summary>
        /// Writes a ReloadNode and its associated tree to a file.
        /// </summary>
        /// <param name="reloadFileLocation">The file to write the ReloadNode to.</param>
        /// <param name="inNode">The ReloadNode to write.</param>
        /// <remarks>This will overwrite the target file. Make sure you know what you're doing!</remarks>
        public static void writeReloadFile(string reloadFileLocation, ReloadNode inNode)
        {
            List<string> names = new List<string>();
            int stringMember = 1;
            int stringTableLocation;

            using (FileStream outFile = new FileStream(reloadFileLocation, FileMode.Create))
            {
                using (BinaryWriter b = new BinaryWriter(outFile))
                {
                    //Write the preamble first
                    b.Write(Reload.MagicNumber);
                    b.Write(Reload.VersionNumber);
                    b.Write(Reload.HeaderSize);
                    b.Write((int)0); //We'll get back to the string table soon.

                    //now the node content
                    writeOut(b, inNode);

                    //After that, define where the string table is
                    stringTableLocation = (int)b.BaseStream.Position;
                    b.BaseStream.Seek(9, SeekOrigin.Begin);
                    b.Write(stringTableLocation);

                    //and then go back to where we'll write the string data
                    b.BaseStream.Seek(stringTableLocation, SeekOrigin.Begin);

                    foreach (string nodeName in names)
                    {
                        writeVLI(b, nodeName.Length);
                        writeString(b, nodeName);
                        stringMember++;
                    }

                }
            }

            void writeOut(BinaryWriter bw, ReloadNode node)
            {
                long startLocation = bw.BaseStream.Position;
                long flashbackLocation;
                int nodeLength;

                bw.Write((int)0); //We'll get back to the length later.

                names.Add(node.Name);
                int tablePosition = names.Count;
                writeVLI(bw, tablePosition);

                switch (node.Type)
                {
                    case Reload.NodeType.Null:
                        bw.Write((byte)0);
                        break;
                    case Reload.NodeType.Int:
                        if (node.IntData <= SByte.MaxValue && node.IntData >= SByte.MinValue)
                        {
                            bw.Write((byte)1);
                            bw.Write((sbyte)node.IntData);
                        }
                        else if (node.IntData <= Int16.MaxValue && node.IntData >= Int16.MinValue)
                        {
                            bw.Write((byte)2);
                            bw.Write((short)node.IntData);
                        }
                        else if (node.IntData <= Int32.MaxValue && node.IntData >= Int32.MinValue)
                        {
                            bw.Write((byte)3);
                            bw.Write((int)node.IntData);
                        }
                        else //It'll be a 64-bit value if it fell through the other conditions
                        {
                            bw.Write((byte)4);
                            bw.Write(node.IntData);
                        }
                        break;
                    case Reload.NodeType.Double:
                        bw.Write((byte)5);
                        bw.Write(node.DoubleData);
                        break;
                    case Reload.NodeType.String:
                        bw.Write((byte)6);
                        writeString(bw, node.StringData);
                        break;
                    default:
                        Debug.WriteLine("Something went wrong!");
                        break;
                }

                writeVLI(bw, node.Children.Count);

                foreach (ReloadNode child in node.Children)
                {
                    writeOut(bw, child);
                }

                flashbackLocation = bw.BaseStream.Position;
                nodeLength = (int)(flashbackLocation - startLocation) - 4;
                bw.BaseStream.Seek(startLocation, SeekOrigin.Begin);
                bw.Write(nodeLength);
                bw.BaseStream.Seek(flashbackLocation, SeekOrigin.Begin);
            }
        }

        //TODO: write methods to export a ReloadNode to YAML structure (use nohrio format as standard?)

        private static void writeString(BinaryWriter bw, string s)
        {
            writeVLI(bw, s.Length);
            char[] charArray = s.ToCharArray();
            foreach (char c in charArray)
            {
                bw.Write((byte)c);
            }
        }

        // VLI methods
        /*
         * A VLI is a variable-length integer.
         * 
         * The first byte is of the format:
         * CNxxxxxx
         * where C is a flag that indicates whether there are more bytes in the VLI and N is a negation flag.
         * 
         * Subsequent bytes are of the format
         * Cxxxxxxx.
         * 
         */


        /// <summary>
        /// Reads a variable-length integer from a file.
        /// </summary>
        /// <param name="br">The BinaryReader pointing to the file to write to.</param>
        /// <returns></returns>
        private static int readVLI(BinaryReader br)
        {
            bool isNegative = false;
            int b = 0;
            int c = 0;
            int offset = 6;
            int result = 0;

            b = br.ReadByte();
            if ((b & 0x40) != 0) isNegative = true;
            result = b & 0x3F;

            while ((b & 0x80) != 0)
            {
                b = br.ReadByte();
                c = b & 0x7F;
                c = c << offset;
                result += c;
                offset += 7;
            }

            if (isNegative) result = result * -1;

            return result;
        }

        /// <summary>
        /// Writes a integer to a file as a variable-length integer.
        /// </summary>
        /// <param name="bw">The BinaryWriter pointing to the file to write to.</param>
        /// <param name="num">The integer to write as a VLI.</param>
        private static void writeVLI(BinaryWriter bw, int num)
        {
            bool isNegative = (num < 0);
            bool isSingleByte = true;
            int a = num;
            int b;
            if (isNegative) a = Math.Abs(a);

            b = a & 0x3f;
            if (isNegative) b += 0x40;
            if (a >= 0x40)
            {
                b += 0x80;
                isSingleByte = false;
            }

            bw.Write(b);

            a = a >> 6;

            //if the number is a single byte, this is skipped
            while (a > 0x80)
            {
                b = (a & 0x7f);
                b += 0x80;
                bw.Write(b);
                a = a >> 7;
            }
            //we write the last byte of a multi-byte VLI here.
            if (!isSingleByte)
            {
                b = (a & 0x7f);
                bw.Write(b);
            }

        }

    }

}
