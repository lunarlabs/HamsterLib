﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HamsterLib
{
    /// <summary>
    /// An RPG file.
    /// </summary>
    public class RPG
    {
        #region file details, information, and file procedures

        private static string _fileLocation;
        private string _folderLocation;
        private bool _isLocked;

        public RPG()
        {
            //if there's something that all RPGs need, put it here...
        }

        public static string FileLocation { get => _fileLocation; }
        public string FolderLocation { get => _folderLocation; }
        public bool IsLocked { get => _isLocked; }

#if DEBUG
        public static ushort PasswordHash(string password)
#else
        private static ushort PasswordHash(string password)
#endif
        {
            if (password.Length == 0) return 0;
            ushort result = 0;
            foreach (char c in password.ToCharArray())
            {
                result = (ushort)(result * 3 + c * 31);
            }
            return (ushort)((result & 511) | 512);
        }

        #endregion
        #region RPG Game Data
        private List<Hero> _heroes;
        private List<Item> _items;
        private List<Attack> _attacks;
        private List<Maps.Map> _maps;

        #endregion
    }
}
