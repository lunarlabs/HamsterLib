﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HamsterLib.Maps
{
    public class Map
    {
        private bool _minimapAvailable, _saveAnywhere;
        private EdgeMode _edgeMode;
        private short _defaultTileset, _ambientMusic, _nameDisplayTime, _defaultEdgeTile, 
            _autorunScript, _scriptArg, _harmTileDamage, _harmTileFlash, _footOffset, 
            _afterBattleScript, _insteadOfBattleScript, _eachStepScript, _onKeypressScript, 
            _npcInstances, _mapLayersBelowActors, _defaultNPCMovementZone, 
            _defaultNPCAvoidZone, _savXoffset, _savYoffset;
        private short[] _tilesets = new short[8];
        private string[] _mapLayerNames = new string[8];

        //this is stored in .Txx, not .MAP
        private short _width, _height;

        //this is stored in .MN, not .MAP
        private string _name;

        public const short MinWidth = 16;
        public const short MinHeight = 10;

    }

    public struct Tile
    {
        public short x, y;
        public Tile(short p1, short p2)
        {
            x = p1;
            y = p2;
        }
    }

    class NPCDefinition
    {
        private SpriteReference _sprRef;
        private short _displayTextBox, _giveItem, _appearanceTag1, _appearanceTag2, _scriptTrigger,
            _scriptArg, _vehicleType, _zoneRestriction, _zoneAvoid;
        private bool _ignorePassMap;
        private NPCMoveType _moveType;
        private NPCSpeed _speed;
        private NPCWhenActivated _whenActivated;
        private NPCPushability _pushability;
        private NPCActivatedBy _activatedBy;

        public SpriteReference SprRef { get => _sprRef; set => _sprRef = value; }
        public short DisplayTextBox { get => _displayTextBox; set => _displayTextBox = value; }
        public short GiveItem { get => _giveItem; set => _giveItem = value; }
        public short AppearanceTag1 { get => _appearanceTag1; set => _appearanceTag1 = value; }
        public short AppearanceTag2 { get => _appearanceTag2; set => _appearanceTag2 = value; }
        public short ScriptTrigger { get => _scriptTrigger; set => _scriptTrigger = value; }
        public short ScriptArg { get => _scriptArg; set => _scriptArg = value; }
        public short VehicleType { get => _vehicleType; set => _vehicleType = value; }
        public short ZoneRestriction { get => _zoneRestriction; set => _zoneRestriction = value; }
        public short ZoneAvoid { get => _zoneAvoid; set => _zoneAvoid = value; }
        public bool IgnorePassMap { get => _ignorePassMap; set => _ignorePassMap = value; }
        public NPCMoveType MoveType { get => _moveType; set => _moveType = value; }
        public NPCSpeed Speed { get => _speed; set => _speed = value; }
        public NPCWhenActivated WhenActivated { get => _whenActivated; set => _whenActivated = value; }
        public NPCPushability Pushability { get => _pushability; set => _pushability = value; }
        public NPCActivatedBy ActivatedBy { get => _activatedBy; set => _activatedBy = value; }
    }

    class NPCInstance
    {
        private short x, y, id;
        private NPCDirection _direction;

        public short X { get => x; set => x = value; }
        public short Y { get => y; set => y = value; }
        public short Id { get => id; set => id = value; }
        public NPCDirection Direction { get => _direction; set => _direction = value; }
    }

    class Grid
    {
        private byte[,] _byteArray;

        public Grid(short width, short height)
        {
            _byteArray = new byte[width, height];
        }

        public Grid(byte[,] inArray)
        {
            _byteArray = inArray;
        }

        public short Width
        {
            get
            {
                return (short)(_byteArray.GetUpperBound(0) + 1);
            }
        }

        public short Height
        {
            get
            {
                return (short)(_byteArray.GetUpperBound(1) + 1);
            }
        }

        public byte GetTileAt(short x, short y)
        {
            if ((x < Width && y < Height) && (x >= 0 && y >= 0))
            {
                return _byteArray[x, y];
            }
            else
            {
                throw new IndexOutOfRangeException("Tile (" + x + ", " + y + ") is outside the " + 
                    Width + "x" + Height + " grid dimensions.");
            }
        }

        public void SetTileAt(short x, short y, byte data)
        {
            if ((x < Width && y < Height) && (x >= 0 && y >= 0))
            {
                _byteArray[x, y] = data;
            }
            else
            {
                throw new IndexOutOfRangeException("Tile (" + x + ", " + y + ") is outside the " +
                    Width + "x" + Height + " grid dimensions.");
            }
        }

        public void HorizontalShift(int amount)
        {
            int w = Width;
            int h = Height;
            byte[,] newArray = new byte[w,h];
            if (amount >= 0)
            {
                for (int x = 0; x < (w - amount); x++)
                {
                    for (int y = 0; y < h; y++)
                    {
                        newArray[(x + amount), y] = _byteArray[x, y];
                    }
                }
            }else if(amount < 0)
            {
                for (int x = amount; x < w; x++)
                {
                    for (int y = 0; y < h; y++)
                    {
                        newArray[(x - amount), y] = _byteArray[x, y];
                    }
                }
            }
            _byteArray = newArray;
        }

        public void VerticalShift(int amount)
        {
            byte[,] newArray = new byte[Width, Height];
            throw new NotImplementedException();
            if (amount >= 0)
            {

            }
            else if (amount < 0)
            {

            }
            _byteArray = newArray;
        }

        public void Resize(short newWidth, short newHeight)
        {
            if ((newWidth >= Map.MinWidth) && (newHeight >= Map.MinHeight))
            {
                byte[,] newArray = new byte[newWidth, newHeight];
                int xCopies = Math.Min(Width, newWidth);
                int yCopies = Math.Min(Height, newHeight);
                for (int x = 0; x < xCopies; x++)
                {
                    for (int y = 0; y < yCopies; y++)
                    {
                        newArray[x, y] = _byteArray[x, y];
                    }
                }
                _byteArray = newArray;
            }
            else
            {
                throw new IndexOutOfRangeException("Grid size needs to be at least" + Map.MinWidth + "x" + Map.MinHeight);
            }
        }

    }

    public enum EdgeMode
    {
        Crop, Wrap, DefaultEdgeTile
    }

    public enum HeroNPCDrawOrder
    {
        HeroesOverNPCs, NPCsOverHeroes, Together
    }

    public enum LumpLoadingMethod
    {
        LoadStateFile, LoadAndSaveStateFile, IgnoreStateFile
    }

    [Flags]
    public enum Passability
    {
        None = 0,
        North = 1<<0,
        East = 1<<1,
        South = 1<<2,
        West = 1<<3,
        VehicleA = 1<<4,
        VehicleB = 1<<5,
        Harm = 1<<6,
        Overhead = 1<<7
    }

    public enum NPCMoveType
    {
        StandStill,
        Wander,
        Pace,
        RightTurns,
        LeftTurns,
        RandomTurns,
        ChasePlayerMeander,
        AvoidPlayerMeander,
        WalkInPlace,
        ChasePlayerDirect,
        AvoidPlayerDirect,
        FollowWallsRight,
        FollowWallsLeft,
        FollowWallsRightAndStopForOthers,
        FollowWallsLeftAndStopForOthers
    }

    public enum NPCSpeed
    {
        Zero, One, Two, Ten, Four, Five
    }

    public enum NPCWhenActivated
    {
        ChangeDirection, FacePlayer, DontFacePlayer
    }

    public enum NPCPushability
    {
        Off, Full, HorizOnly, VertOnly, UpOnly, RightOnly, DownOnly, LeftOnly
    }

    public enum NPCActivatedBy
    {
        Use, Touch, StepOn
    }

    public enum NPCDirection
    {
        Up, Right, Down, Left
    }
}
