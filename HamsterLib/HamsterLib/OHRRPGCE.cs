﻿/*
 * OHRRPGCE Constants File
 * -----------------------
 * 
 * Most of this stuff is lifted from const.bi in the engine source code and stuff in the wiki.
 * I'm also using nohrio as a source of information/inspiration.
 * 
 * Formal Specs on the Wiki
 * ------------------------
 * 
 * When the wiki refers to an INT or LONG, it's not referring to the Int32 and Int64 C# uses. 
 * Instead, INT is Int16 (short) and LONG is Int32 (int).
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HamsterLib
{
    /// <summary>
    /// Constants and enums used by the OHRRPGCE engine.
    /// </summary>
    public static class OHRRPGCE
    {
        //Version constants

        /// <summary>
        /// The version of OHRRPGCE that the current version of HamsterLib is compatible with.
        /// </summary>
        public const string CompatibleVersion = "callipygous+1";

        /// <summary>
        /// The version of the RPG file format that is being used.
        /// </summary>
        /// <remarks>
        /// This is usually incremented when saving an RPG file in an earlier version would cause data loss.
        /// </remarks>
        public const int CurrentRpgVersion = 20;

        /// <summary>
        /// The RSAV version the engine uses.
        /// </summary>
        /// <remarks>
        /// This is usually incremented when there's major new features.
        /// </remarks>
        public const int CurrentRsavVersion = 3;

        //TODO: See if we really need CURRENT_TESTING_IPC_VERSION in this code
        //public const int CurrentTestingIPCVersion = 4;

        public const int CurrentHSZVersion = 3;
        public const int CurrentHspVersion = 1;
        public const string RecommendedHSpeakVersion = "3S ";

        public const byte bsaveMagicNumber = 0xfd;
        public const ushort bsaveSegment = 0x9999;
        public const ushort bsaveOffset = 0x0;

        /// <summary>
        /// Specifies the stat used in the battle system.
        /// </summary>
        public enum Stat
        {
            HP, MP, Attack, Aim, Defense, Dodge, Magic, Will, Speed, Counter, MPCost, ExtraHits
        }

        /// <summary>
        /// The index number of the last stat used.
        /// </summary>
        public const int LastStat = 11;

        public enum AtHeroLevel
        {
            Level0, LevelCap
        }

        /*
        public enum CameraMode
        {
            Hero, NPC, Pan, Focus, Slice, Stop = -1
        }
        */
    }

    /// <summary>
    /// A reference to a sprite and palette in the OHR system.
    /// </summary>
    public struct SpriteReference
    {
        /// <summary>
        /// The sprite ID number.
        /// </summary>
        public short Sprite;

        /// <summary>
        /// The palette ID number, or -1 if using the sprite's "default" palette.
        /// </summary>
        public short Palette;

        /// <summary>
        /// Creates a new SpriteReference.
        /// </summary>
        /// <param name="p1">The sprite ID number to use.</param>
        /// <param name="p2">The palette ID to use, or -1 to use the sprite default.</param>
        public SpriteReference(short p1, short p2)
        {
            Sprite = p1;
            Palette = p2;
        }
    }

    //TODO: Write all the enums and crap
    /// <summary>
    /// The definitions of the "file format fix bits" in FIXBITS.BIN
    /// </summary>
    [Flags]
    public enum FixBits
    {
        None = 0,
        FixAttackItems              = 0x0000001,
        FixWeapPoints               = 0x0000002,
        FixStunCancelTarg           = 0x0000004,
        FixDefaultDissolve          = 0x0000008,
        FixDefaultDissolveEnemy     = 0x0000010,
        FixPushNPCBugCompat         = 0x0000020,
        FixDefaultMaxItem           = 0x0000040,
        FixBlankDoorLinks           = 0x0000080,
        FixShopSounds               = 0x0000100,
        FixExtendedNPCs             = 0x0000200,
        FixHeroPortrait             = 0x0000400,
        FixTextBoxPortrait          = 0x0000800,
        FixNPCLocationFormat        = 0x0001000,
        FixInitDamageDisplay        = 0x0002000,
        FixDefaultLevelCap          = 0x0004000,
        FixHeroElementals           = 0x0008000,
        FixOldElementalFailBit      = 0x0010000,
        FixAttackElementFails       = 0x0020000,
        FixEnemyElementals          = 0x0040000,
        FixItemElementals           = 0x0080000,
        FixNumElements              = 0x0100000,
        FixRemoveDamageMP           = 0x0200000,
        FixDefaultMaxLevel          = 0x0400000,
        FixUNUSED23                 = 0x0800000,
        FixWipeGEN                  = 0x1000000,
        FixSetOldAttackPosBit       = 0x2000000,
        FixWrapCroppedMapsBit       = 0x4000000,
        FixInitNonElementalSpawning = 0x8000000
    }

    //These are stored as an Int16 in the GEN file.
    /// <summary>
    /// Runtime flags for suspending things in gameplay.
    /// </summary>
    [Flags]
    public enum SuspendBits
    {
        None = 0,

        /// <summary>
        /// Suspend NPC movement.
        /// </summary>
        SuspendNPCs          = 0x0001,

        /// <summary>
        /// Suspend player control except for advancing boxes.
        /// </summary>
        SuspendPlayer        = 0x0002,

        /// <summary>
        /// NPCs and heroes can walk through each other.
        /// </summary>
        SuspendObstructions  = 0x0004,

        /// <summary>
        /// Heroes can walk through walls.
        /// </summary>
        SuspendHeroWalls     = 0x0008,

        /// <summary>
        /// NPCs can walk through walls.
        /// </summary>
        SuspendNPCWalls      = 0x0010,

        /// <summary>
        /// The party heroes no longer follow the first hero.
        /// </summary>
        SuspendCaterpillar   = 0x0020,

        /// <summary>
        /// Random battles won't happen.
        /// </summary>
        SuspendRandomEnemies = 0x0040,

        /// <summary>
        /// The player can't advance message boxes.
        /// </summary>
        SuspendBoxAdvance    = 0x0080,

        /// <summary>
        /// Moves "overlay" tiles to appear behind actors.
        /// </summary>
        SuspendOverlay       = 0x0100,

        /// <summary>
        /// Map music will not play when switching maps.
        /// </summary>
        SuspendAmbientMusic  = 0x0200
    }

    [Flags]
    public enum GenBits
    {
        None = 0,
        PauseOnItemAndSpellMenus   = 0x0001,
        EnableCaterpillar          = 0x0002,
        DontRestoreHpOnLevelUp     = 0x0004,
        DontRestoreMpOnLevelUp     = 0x0008,
        InnsDontRevive             = 0x0010,
        HeroSwapAlwaysAvailable    = 0x0020,
        HideReadyMeterInBattle     = 0x0040,
        HideHealthMeterInBattle    = 0x0080,
        DisableDebugKeys           = 0x0100,
        SimulateOldLevelUpBug      = 0x0200,
        PermitDoubleTrigger        = 0x0400,
        SkipTitleScreen            = 0x0800,
        SkipLoadScreen             = 0x1000,
        PauseOnAllMenus            = 0x2000,
        DisableHeroBattleCursor    = 0x4000,
        DefaultPassabilityDisabled = 0x8000,
    }

    //These are stored in an Int32.
    [Flags]
    public enum MoreGenBits
    {
        None = 0,
        SimulatePushableNPCBug           = 0x00000001,
        DisableEscToRun                  = 0x00000002,
        DontSaveGameOverLoadGameScripts  = 0x00000004,
        DeadHeroesGetExp                 = 0x00000008,
        CantRearrangeLockedHeroes        = 0x00000010,
        AttackCaptionsPauseMeters        = 0x00000020,
        DontRandomizeBattleMeters        = 0x00000040,
        BattleMenusWaitForAnimations     = 0x00000080,
        BetterScancodesForScripts        = 0x00000100,
        SimulateOldFailVsElement         = 0x00000200,
        ZeroDamageFromImmuneElements     = 0x00000400,
        RecreateMapSlicesOnMapChange     = 0x00000800,
        HarmTilesHurtNonCaterpillar      = 0x00001000,
        AttacksIgnoreExtraHits           = 0x00002000,
        DontDivideExpBetweenHeroes       = 0x00004000,
        DontResetMaxStatsAfterOOBAttack  = 0x00008000,
        DontUseThousandTagLimit          = 0x00010000,
        EnableBug430                     = 0x00020000,
        ShowtextboxHappensImmediately    = 0x00040000,
        PauseBattleWhileTargeting        = 0x00080000,
        OldAttackPositionAtBottomLeft    = 0x00100000,
        WrapMapLayersOverCropMapEdge     = 0x00200000,
        NeverShowScriptTimerInBattle     = 0x00400000,
        DrawBackdropSliceOverScriptLayer = 0x00800000
    }

    public enum GameErrorHandling
    {
        Default, Reserved, ShowAllWarnings, SuppressSomeWarnings, SuppressWeakTypeChecking, DoNotShowNewErrors,
        CorruptionOrIntepreterOnly
    }

    /// <summary>
    /// How GAME handles script errors.
    /// </summary>
    public enum DebugMode
    {
        /// <summary>
        /// Specifies Release mode, where script errors are not shown.
        /// </summary>
        Release,

        /// <summary>
        /// Specifies Debug mode, where script errors are shown.
        /// </summary>
        Debug
    }

    public enum ScriptErrorType
    {
        Information, Warning, SuspiciousOperation, Bounds, BadOperation, Error, EngineBug
    }

    /// <summary>
    /// The way the game engine handles equipment elemental resistances.
    /// </summary>
    public enum EquipmentElementMath
    {
        /// <summary>
        /// The old buggy way. Not recommended.
        /// </summary>
        Stupid,

        /// <summary>
        /// Equipment resistances are added to the base resistance.
        /// </summary>
        Additive,

        /// <summary>
        /// Equipment resistances are multipliers.
        /// </summary>
        Multiplicative
    }

    /// <summary>
    /// Specifies what the "AUTOSORT" option does in the item menu.
    /// </summary>
    public enum InventoryAutosortMethod
    {
        /// <summary>
        /// Items are sorted by type.
        /// </summary>
        ByType,

        /// <summary>
        /// Items are sorted by whether or not they are usable.
        /// </summary>
        ByUsability,

        /// <summary>
        /// Items are sorted alphabetically by name.
        /// </summary>
        Alphabetically,

        /// <summary>
        /// Items are sorted by internal ID number.
        /// </summary>
        ByID,

        /// <summary>
        /// No sorting method, just compact item stacks and remove blank spots.
        /// </summary>
        JustRemoveBlanks
    }
}
