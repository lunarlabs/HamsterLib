﻿/*
 * OHRRPGCE Item - Stored in the archinym'd file w/ extension .ITM.
 * In a future release (dwimmercrafty?) that file will be replaced by items.reld.
 * 
 * One of these days I'll have to stop just writing definitions and focus on actually writing
 * file reading methods...
 * 
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HamsterLib
{
    class Item
    {
        private string _name;
        private string _info;
        private int _value;
        private int _inBattleAttack; // attack number plus one
        private int _weaponAttack;   // ditto here
        private ItemEquippableAs _equipAs;
        private int _teachesSpell; // as with _inBattleAttack and _weaponAttack, this is attack number plus one
        private int _outOfBattleAction; // if positive, this is an attack number plus one. If negative, calls
                                        // text box numbered by the abs of this variable.
        private int _weaponPicture;
        private int _weaponPalette; // -1 for default.
        private int[] _statBonuses = new int[OHRRPGCE.LastStat];
        private bool[] _canEquip = new bool[60];
#if USE_OLD_ITEM_RESISTANCE_BOOLS
        private bool[] _elemWeaknesses = new bool[8];
        private bool[] _elemStrengths = new bool[8];
        private bool[] _elemAbsorptions = new bool[8];
#endif
        private int _ownItemTag;
        private int _inInventoryTag;
        private int _isEquippedTag;
        private int _activeHeroEquipTag;
        private Point _frameAHandle;
        private Point _frameBHandle;
        private double[] _elementalResistances = new double[64];
        private int _maxInvStackSize; // 0 means default

        public string Name { get => _name; set => _name = value; }
        public string Info { get => _info; set => _info = value; }
        public int Value { get => _value; set => _value = value; }
        public int InBattleAttack { get => _inBattleAttack; set => _inBattleAttack = value; }
        public int WeaponAttack { get => _weaponAttack; set => _weaponAttack = value; }
        public int TeachesSpell { get => _teachesSpell; set => _teachesSpell = value; }
        public int OutOfBattleAction { get => _outOfBattleAction; set => _outOfBattleAction = value; }
        public int WeaponPicture { get => _weaponPicture; set => _weaponPicture = value; }
        public int WeaponPalette { get => _weaponPalette; set => _weaponPalette = value; }
        public int[] StatBonuses { get => _statBonuses; }
        public bool[] CanEquip { get => _canEquip; }
#if USE_OLD_ITEM_RESISTANCE_BOOLS
        public bool[] ElemWeaknesses { get => _elemWeaknesses; set => _elemWeaknesses = value; }
        public bool[] ElemStrengths { get => _elemStrengths; set => _elemStrengths = value; }
        public bool[] ElemAbsorptions { get => _elemAbsorptions; set => _elemAbsorptions = value; }
#endif
        public int OwnItemTag { get => _ownItemTag; set => _ownItemTag = value; }
        public int InInventoryTag { get => _inInventoryTag; set => _inInventoryTag = value; }
        public int IsEquippedTag { get => _isEquippedTag; set => _isEquippedTag = value; }
        public int ActiveHeroEquipTag { get => _activeHeroEquipTag; set => _activeHeroEquipTag = value; }
        public Point FrameAHandle { get => _frameAHandle; set => _frameAHandle = value; }
        public Point FrameBHandle { get => _frameBHandle; set => _frameBHandle = value; }
        public double[] ElementalResistances { get => _elementalResistances; }
        public int MaxInvStackSize { get => _maxInvStackSize; set => _maxInvStackSize = value; }
        public ItemEquippableAs EquipAs { get => _equipAs; set => _equipAs = value; }
    }

    enum ItemEquippableAs
    {
        NeverEquipped,
        Weapon,
        Armor1,
        Armor2,
        Armor3,
        Armor4
    }

    enum ItemConsumability
    {
        Unlimited,
        ConsumedByUse,
        CantDropOrSell
    }
}
