﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace HamsterLib
{
    public static class RPGLumpManager
    {
        // private const long[] PDPOffsets = { 256 ^ 2, 256 ^ 3, 256 ^ 0, 256 ^ 1 };
        // Only used here for some reason. Used to get the length of lumps.

        // PDP endianness: high, highest, lowest, low

        /// <summary>
        /// Converts a lumped file into its component files.
        /// </summary>
        /// <param name="sourceFileLocation">The file to unlump.</param>
        /// <param name="destinationDirectory">The directory to unlump to.</param>
        public static void unLump(string sourceFileLocation, string destinationDirectory)
        {
            bool _endOfFile = false;
            string _fileName;
            byte b;
            byte[] _PDPlength;
            int _length;

            if (System.IO.File.Exists(sourceFileLocation))
            {
                Debug.WriteLine("Unlumping " + Path.GetFileName(sourceFileLocation) + " to "
                    + destinationDirectory + "...");
                //Debug.Indent();
                if (Directory.Exists(destinationDirectory))
                {
                    Debug.WriteLine("Directory exists, files may be overwritten.");
                }
                else
                {
                    Directory.CreateDirectory(destinationDirectory);
                }

                using (FileStream inFile = new FileStream(sourceFileLocation, FileMode.Open))
                {
                    if (inFile.Length == 0)
                    {
                        throw new EndOfStreamException("File is zero bytes");
                    }
                    using (BinaryReader r = new BinaryReader(inFile))
                    {
                        while (_endOfFile == false)
                        {

                            StringBuilder extractedFileName = new StringBuilder(50);
                            //read the file name until we hit NUL

                            b = r.ReadByte();
                            do
                            {
                                if (inFile.Position == inFile.Length)
                                {
                                    throw new EndOfStreamException("Reached the end of the file while reading file name");
                                }
                                extractedFileName.Append((char)b);
                                b = r.ReadByte();
                            } while (b > 0);
                            _fileName = Path.GetFullPath(destinationDirectory) + Path.DirectorySeparatorChar
                                + extractedFileName.ToString();
#if VERBOSE
                            Debug.Write("Unlumping file " + _fileName);
#endif
                            //read the length and translate it from PDP-endian to proper format
                            _PDPlength = r.ReadBytes(4);
                            if (_PDPlength.Length != 4)
                            {
                                throw new EndOfStreamException("Reached the end of the file while reading file length");
                            }
                            _length = convertLength(_PDPlength);
#if VERBOSE
                            Debug.Write(", " + _length + " bytes... ");
#endif

                            //read the file for the length specified
                            using (FileStream outFile = new FileStream(_fileName, FileMode.Create))
                            {
                                using (BinaryWriter w = new BinaryWriter(outFile))
                                {
                                    for (int i = 0; i < _length; i++)
                                    {
                                        w.Write(r.ReadByte());
                                    }
#if VERBOSE
                                    Debug.WriteLine("done.");
#endif
                                }
                            }
                            if (inFile.Position == inFile.Length)
                            {
                                _endOfFile = true;
                            }

                        }
                    }
                }
                //Debug.Unindent();
                Debug.WriteLine("Finished unlumping!");

            }
            else
            {
                throw new FileNotFoundException("Can't find the lump file: " + sourceFileLocation);
            }
        }

        private static int convertLength(byte[] PDPvalue)
        {
            int result;
            byte[] convertedLength = { PDPvalue[2], PDPvalue[3], PDPvalue[0], PDPvalue[1] };
            result = BitConverter.ToInt32(convertedLength, 0);
            return result;
        }

        /// <summary>
        /// Converts a lumped file into its component files, using a default directory
        /// </summary>
        /// <param name="sourceFileLocation">The file to unlump.</param>
        public static void unLump(string sourceFileLocation)
        {
            Debug.WriteLine("No destination directory specified, unlumping to " + Path.GetFileName(sourceFileLocation)
                + ".hwtmp...");
            string tempDir = sourceFileLocation + ".hwtmp";
            unLump(sourceFileLocation, tempDir);
        }

        public static void lump(string sourceDirectory, string lumpDestination, bool deleteAfterwards)
        {
            throw new NotImplementedException();
        }
    }

    public class LumpFileReader : BinaryReader
    {
        public LumpFileReader(Stream str) : base(str) { }
        // if encoding is really really needed, we'll consider the other constructors

        public enum StringDataFormat
        {
            Int8, Int16
        }

        public string ReadVSTR(StringDataFormat lengthFormat, StringDataFormat dataFormat)
        {
            short length;
            switch (lengthFormat)
            {
                case StringDataFormat.Int8:
                    length = base.ReadByte();
                    break;
                case StringDataFormat.Int16:
                    length = base.ReadInt16();
                    break;
                default:
                    throw new FormatException();
            }
            StringBuilder sb = new StringBuilder(length);
            short c;
            for (short i = 0; i < length; i++)
            {
                switch (dataFormat)
                {
                    case StringDataFormat.Int8:
                        c = base.ReadByte();
                        break;
                    case StringDataFormat.Int16:
                        c = base.ReadInt16();
                        break;
                    default:
                        throw new FormatException();
                }
                sb.Append((char)c);
            }
            return sb.ToString();
        }

        public void WriteVSTR(string toWrite, StringDataFormat lengthFormat, StringDataFormat dataFormat)
        {
            throw new NotImplementedException();
        }

        public string ReadFVSTR(StringDataFormat lengthFormat, StringDataFormat dataFormat, int length)
        {
            string result = ReadVSTR(lengthFormat, dataFormat);
            int gap = length - result.Length;
            if (dataFormat == StringDataFormat.Int16) gap *= 2;
            base.BaseStream.Seek(gap, SeekOrigin.Current);
            return result;
        }

        public void WriteFVSTR(string toWrite, StringDataFormat lengthFormat, StringDataFormat dataFormat, int length)
        {
            throw new NotImplementedException();
        }

        public string ReadZSTR()
        {
            byte c = base.ReadByte();
            StringBuilder sb = new StringBuilder();
            while (c != 0)
            {
                sb.Append((char)c);
                c = base.ReadByte();
            }
            return sb.ToString();
        }

        public void WriteZSTR(string toWrite)
        {
            throw new NotImplementedException();
        }
    }
}
