﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HamsterLib
{
    class Enemy
    {
        private string _name;
        private EnemyTheivability _theivability;
        private short _stealableItem;
        private short _stealChance;
        private short _rareStealableItem;
        private short _rareStealChance;
        private EnemyDissolveAnimation _dissolveAnimation;
        private short _dissolveTime;
        private short _deathSFX;
        private Point _battleCursorOffset;
        private SpriteReference _sprite;
        private EnemySpriteSize _spriteSize;
        private short _moneyReward;
        private short _expReward;
        private short _itemReward;
        private short _itemDropPercentage;
        private short _rareItem;
        private short _rareItemPercentage;
        private short[] _stats = new short[OHRRPGCE.LastStat + 1];
        //TODO: add bitsets after writing Bitset Class
        private short[] _enemyToSpawn = new short[4];
        private short[] _enemyToSpawnOnElementalHit = new short[64];
        private short _enemiesToSpawn;
        private short[] _regularAttacks = new short[5];
        private short[] _desperationAttacks = new short[5];
        private short[] _aloneAttacks = new short[5];
        private short[] _counterAttackToElementalHit = new short[64];
        private float[] _elementalDamageMultipliers = new float[64];
        private short _onDeathBequestAttack;
        private short _counterAttackToNonElementalAttack;

    }

    public enum EnemySpawnCondition
    {
        OnDeath, OnNonElementalDeath, WhenAlone, OnNonElementalHit
    }

    public enum EnemySpriteSize
    {
        Small, Medium, Large
    }

    public enum EnemyTheivability
    {
        UnThievable = -1,
        OnceOnly,
        InfinitelyThievable
    }

    public enum EnemyDissolveAnimation
    {
        Default,
        RandomPixels,
        Crossfade,
        DiagonalVanish,
        SinkIntoGround,
        Squash,
        Melt,
        Vaporize,
        PhaseOut,
        Squeeze,
        Shrink,
        Flicker
    }

    [Flags]
    public enum EnemyBitsets : ulong
    {
        None=0,
        HarmedByCure = 1uL << 22,
        MPIdiot = 1uL << 23,
        Boss = 1uL << 24,
        Unescapable = 1uL << 25,
        DieWithoutBoss = 1uL << 26,
        FleeInsteadOfDie = 1uL << 27,
        UntargetableByEnemies = 1uL << 28,
        UntargetableByHeroes = 1uL << 29,
        WinEvenIfAlive = 1uL << 30,
        NeverFlinchWhenHit = 1uL << 31,
        AloneAiIgnores = 1uL << 32,
    }
}
