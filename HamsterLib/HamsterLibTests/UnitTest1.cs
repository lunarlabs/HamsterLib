﻿using System;
using HamsterLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Diagnostics;
using System.Threading;

namespace HamsterLibTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestUnlumpFile()
        {
            string halowoofLocation = AppDomain.CurrentDomain.BaseDirectory + Path.DirectorySeparatorChar + @"halowoof.rpg";
            Debug.WriteLine(halowoofLocation);
            RPGLumpManager.unLump(halowoofLocation);
        }

        //TODO: Pass hash is in element 92 -> 0x00BF
        [TestMethod]
        public void TestPasswordProtect()
        {
            string password = "abc123def456";
            ushort expectedResult = 0x036c;
            ushort badResult = 0;
            ushort result;
            result = RPG.PasswordHash(password);
            Assert.AreEqual(expectedResult, result, "Password hashes don't match");
            Assert.AreNotEqual(badResult, result, "Something went wrong with the password hash method.");
        }

        [TestMethod]
        public void TestReloadFileOps()
        {
            Stopwatch stopWatch = new Stopwatch();
            string reload1Location = AppDomain.CurrentDomain.BaseDirectory + Path.DirectorySeparatorChar 
                + @"all_slice_collections.reload";
            string reload2Location = AppDomain.CurrentDomain.BaseDirectory + Path.DirectorySeparatorChar
                + @"plotdict_repeated.reload";
            string reload3Location = AppDomain.CurrentDomain.BaseDirectory + Path.DirectorySeparatorChar
                + @"powerxe_backdrops.rgfx";
            string reload1OutLocation = AppDomain.CurrentDomain.BaseDirectory + Path.DirectorySeparatorChar
                + @"all_slice_collections.test.reload";
            string reload2OutLocation = AppDomain.CurrentDomain.BaseDirectory + Path.DirectorySeparatorChar
                + @"plotdict_repeated.test.reload";
            string reload3OutLocation = AppDomain.CurrentDomain.BaseDirectory + Path.DirectorySeparatorChar
                + @"powerxe_backdrops.test.rgfx";
            #region Stopwatch Time Properties
            if (Stopwatch.IsHighResolution)
            {
                Console.WriteLine("Operations timed using the system's high-resolution performance counter.");
            }
            else
            {
                Console.WriteLine("Operations timed using the DateTime class.");
            }

            long frequency = Stopwatch.Frequency;
            Console.WriteLine("  Timer frequency in ticks per second = {0}",
                frequency);
            long nanosecPerTick = (1000L * 1000L * 1000L) / frequency;
            Console.WriteLine("  Timer is accurate within {0} nanoseconds",
                nanosecPerTick);
            #endregion
            Console.WriteLine("Now extracting " + reload1Location);
            stopWatch.Start();
            ReloadNode node1 = ReloadFileIO.readReloadFile(reload1Location);
            stopWatch.Stop();
            Console.WriteLine("Extract time (ms): " + stopWatch.ElapsedMilliseconds);
            Console.WriteLine("Writing to: " + reload1OutLocation);
            stopWatch.Restart();
            ReloadFileIO.writeReloadFile(reload1OutLocation, node1);
            stopWatch.Stop();
            Console.WriteLine("Write time (ms): " + stopWatch.ElapsedMilliseconds);

            Console.WriteLine("Now extracting " + reload2Location);
            stopWatch.Restart();
            ReloadNode node2 = ReloadFileIO.readReloadFile(reload2Location);
            stopWatch.Stop();
            Console.WriteLine("Extract time (ms): " + stopWatch.ElapsedMilliseconds);
            Console.WriteLine("Writing to: " + reload2OutLocation);
            stopWatch.Restart();
            ReloadFileIO.writeReloadFile(reload2OutLocation, node2);
            stopWatch.Stop();
            Console.WriteLine("Write time (ms): " + stopWatch.ElapsedMilliseconds);

            Console.WriteLine("Now extracting " + reload3Location);
            stopWatch.Restart();
            ReloadNode node3 = ReloadFileIO.readReloadFile(reload3Location);
            stopWatch.Stop();
            Console.WriteLine("Extract time (ms): " + stopWatch.ElapsedMilliseconds);
            Console.WriteLine("Writing to: " + reload3OutLocation);
            stopWatch.Restart();
            ReloadFileIO.writeReloadFile(reload3OutLocation, node3);
            stopWatch.Stop();
            Console.WriteLine("Write time (ms): " + stopWatch.ElapsedMilliseconds);
        }

        [TestMethod]
        public void TestGridOps()
        {
            byte[,] testPattern = new byte[4, 4] { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 }, { 13, 14, 15, 16 } };
        }
    }
}
