﻿/*
 * OHRRPGCE Attack - This is a bit of a weird one.
 * The first 40 shorts of each attack record are stored in the archinym'd file with
 * the extension .DT6, while the remainder is stored in ATTACK.BIN.
 * 
 * There are no plans I know of to replace these files with a RELOAD file.
 * 
 * OH MY GOD SO MANY ENUMS
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HamsterLib
{
    class Attack
    {

    }

    public enum AttackAnimationPattern
    {
        CycleForward, CycleBackward, Oscillate, Random
    }

    public enum AttackTargetClass
    {
        Enemy,
        AllyIncludingDead,
        Self,
        All,
        AllyExcludingDead,
        AllyExcludingSelf,
        RevengeLastHit,
        RevengeWholeBattle,
        PreviousTarget,
        RecordedTarget,
        DeadAllies,
        ThankvengeLastHit,
        ThankvengeWholeBattle
    }

    public enum AttackTargetSetting
    {
        Focused, Spread, OptionalSpread, RandomFocus, FirstTarget
    }

    public enum AttackDamageEquation
    {
        Normal,
        Blunt,
        Sharp,
        PureDamage,
        NoDamage,
        SetTargetToPercentOfMax,
        SetTargetToPercentOfCurrent,
        PercentOfMax,
        PercentOfCurrent
    }

    public enum AttackAimMath
    {
        Normal,
        Poor,
        Bad,
        NeverMisses,
        Magic,
        PercentageAimVsDodge,
        PercentageAim,
        PercentageMagVsWill,
        PercentageMag
    }

    public enum AttackBaseAttackStat
    {
        AttackerAttack,
        AttackerMagic,
        AttackerHP,
        AttackerLostHP,
        Random,
        OneHundred,
        AttackerHP1,
        AttackerMP,
        AttackerAttack1,
        AttackerAim,
        AttackerDefense,
        AttackerDodge,
        AttackerMagic1,
        AttackerWill,
        AttackerSpeed,
        AttackerCounter,
        AttackerFocus,
        AttackerExtraHits,
        PreviousAttack,
        LastDamageToAttacker,
        LastDamageToTarget,
        LastCureToAttacker,
        LastCureToTarget,
        TargetHP,
        TargetMP,
        TargetAttack,
        TargetAim,
        TargetDefense,
        TargetDodge,
        TargetMagic,
        TargetWill,
        TargetSpeed,
        TargetCounter,
        TargetFocus,
        TargetExtraHits,
        AttackerMaxHP,
        AttackerMaxMP,
        AttackerMaxAttack,
        AttackerMaxAim,
        AttackerMaxDefense,
        AttackerMaxDodge,
        AttackerMaxMagic,
        AttackerMaxWill,
        AttackerMaxSpeed,
        AttackerMaxCounter,
        AttackerMaxFocus,
        AttackerMaxExtraHits,
        TargetMaxHP,
        TargetMaxMP,
        TargetMaxAttack,
        TargetMaxAim,
        TargetMaxDefense,
        TargetMaxDodge,
        TargetMaxMagic,
        TargetMaxWill,
        TargetMaxSpeed,
        TargetMaxCounter,
        TargetMaxFocus,
        TargetMaxExtraHits,
    }

    public enum AttackAttackerAnimation
    {
        Strike,
        Cast,
        DashIn,
        SpinStrike,
        Jump,
        Land,
        Null,
        StandingCast,
        Teleport,
        StandingStrike
    }

    public enum AttackAttackAnimation
    {
        Normal,
        Projectile,
        ReverseProjectile,
        Drop,
        Ring,
        Wave,
        Scatter,
        SequentialProjectile,
        Meteor,
        DriveBy,
        Null
    }

    public enum AttackTargetStat
    {
        HP, MP, Attack, Aim, Defense, Dodge, Magic, Will, Speed, Counter, MPCost, ExtraHits,
        Poison, Regen, Stun, Mute
    }

    public enum AttackPreferredTarget
    {
        Default, First, Closest, Farthest, Random, Weakest, Strongest,
        WeakestPercent, StrongestPercent
    }

    [Flags]
    public enum AttackBitsets1 : ulong
    {
        None = 0,
        CureInsteadOfHarm = 1uL<<0,
        DivideSpreadDamage = 1uL<<1,
        AbsorbDamage = 1uL<<2,
        UnreversablePicture = 1uL<<3,
        StealItem = 1uL<<4,
        //5-20 is in the ElementalDamage bool array.
#if ALLOW_OLD_ELEMENT_FAILS
        FailVsElement1 = 1uL<<21,
        FailVsElement2 = 1uL<<22,
        FailVsElement3 = 1uL<<23,
        FailVsElement4 = 1uL<<24,
        FailVsElement5 = 1uL<<25,
        FailVsElement6 = 1uL<<26,
        FailVsElement7 = 1uL<<27,
        FailVsElement8 = 1uL<<28,
        FailVsElement9 = 1uL<<29,
        FailVsElement10 = 1uL<<30,
        FailVsElement11 = 1uL<<31,
        FailVsElement12 = 1uL<<32,
        FailVsElement13 = 1uL<<33,
        FailVsElement14 = 1uL<<34,
        FailVsElement15 = 1uL<<35,
        FailVsElement16 = 1uL<<36,
#endif
        CantTargetEnemy0 = 1uL << 37,
        CantTargetEnemy1 = 1uL << 38,
        CantTargetEnemy2 = 1uL << 39,
        CantTargetEnemy3 = 1uL << 40,
        CantTargetEnemy4 = 1uL << 41,
        CantTargetEnemy5 = 1uL << 42,
        CantTargetEnemy6 = 1uL << 43,
        CantTargetEnemy7 = 1uL << 44,
        CantTargetHero0 = 1uL << 45,
        CantTargetHero1 = 1uL << 46,
        CantTargetHero2 = 1uL << 47,
        CantTargetHero3 = 1uL << 48,
        IgnoreAttackersExtraHits = 1uL << 49,
        EraseEnemyRewards = 1uL << 50,
        ShowDamageWithoutInflicting = 1uL << 51,
        StoreTarget = 1uL << 52,
        DeleteStoreTarget = 1uL << 53,
        AutoChooseTarget = 1uL << 54,
        ShowAttackName = 1uL << 55,
        DontDisplayDamage = 1uL << 56,
        ResetTargetStatToMaxBeforeHit = 1uL << 57,
        AllowCureToExceedMax = 1uL << 58,
        UsableOutOfBattle = 1uL << 59,
        ObsoleteDamageMP = 1uL << 60,
        DontRandomize = 1uL << 61,
        AllowZeroDamage = 1uL << 62,
        MakeHeroesRun = 1uL << 63,
    }

    public enum AttackBaseDefStat
    {
        Default, HP, MP, Attack, Aim, Defense, Dodge, Magic, Will, Speed,
        Counter, MPCost, ExtraHits
    }

    public enum AttackTagCondition
    {
        Never, Always, Hit, Miss, Kill
    }

    //a 128-bit integer?! Really?
    //Okay, when writing the element 17-64 damage bits to file, make a
    //seperate long after AttackBitsets2, and shift it left 16 before writing.
    [Flags]
    public enum AttackBitsets2
    {
        None = 0,
        Mutable = 1 << 0,
        FailIfTargetPoisoned = 1 << 1,
        FailIfTargetRegen = 1 << 2,
        FailIfTargetStunned = 1 << 3,
        FailIfTargetMuted = 1 << 4,
        PercentAttacksDamage = 1 << 5,
        CheckCostsIfUsedAsWeapon = 1 << 6,
        DontChainIfAttackFails = 1 << 7,
        ResetPoisonRegister = 1 << 8,
        ResetRegenRegister = 1 << 9,
        ResetStunRegister = 1 << 10,
        ResetMuteRegister = 1 << 11,
        CancelTargetsAttack = 1 << 12,
        Uncancelable = 1 << 13,
        DontTriggerSpawnOnAttack = 1 << 14,
        DontTriggerSpawnOnDeath = 1 << 15,
        CheckCostsIfUsedAsItem = 1 << 16,
        RecheckCostsAfterDelay = 1 << 17,
        DontMakeTargetFlinch = 1 << 18,
        DontAllowExcessiveDamage = 1 << 19,
        DelayWontBlockFurtherActions = 1 << 20,
        ForceVictory = 1 << 21,
        ForceBattleExit = 1 << 22,
        NeverTriggerCounterAttacks = 1 << 23,
        HealPoisonBeyondMaxMakesRegenAndVV = 1 << 24,
        //25-79 are unused.
        //80-127 are stored in ElementalDamage bool array.
    }

    public enum AttackCompareStat
    {
        Default, HP, MP, Attack, Aim, Defense, Dodge, Magic, Will, Speed, Counter,
        MPCost, ExtraHits, Poison, Regen, Stun, Mute
    }

    public enum AttackChainCondition
    {
        NoConditions,
        CheckTags,
        AttackerVal1GreaterThanVal2,
        AttackerVal1LessThanVal2,
        AttackerVal1GreaterThanVal2Percent,
        AttackerVal1LessThanVal2Percent,
        AnyTargetVal1GreaterThanVal2,
        AnyTargetVal1LessThanVal2,
        AnyTargetVal1GreaterThanVal2Percent,
        AnyTargetVal1LessThanVal2Percent,
        AllTargetVal1GreaterThanVal2,
        AllTargetVal1LessThanVal2,
        AllTargetVal1GreaterThanVal2Percent,
        AllTargetVal1LessThanVal2Percent,
    }

    [Flags]
    public enum AttackChainBits
    {
        None=0,
        AttackerMustKnowChainedAttack = 1 << 0,
        ChainedAttackIgnoresDelay = 1 << 1,
        ChainDelayDoesntBlockActions = 1 << 2,
        DontRetargetIfTargetLost = 1 << 3,
    }

    public enum AttackTransmogrifyStatBehavior
    {
        KeepCurrent,
        RestoreToNewMax,
        PreservePercentage,
        KeepCurrentWithMaxCap
    }
}
